+++
chapter = true
pre = "<b>2.2 </b>"
title = "Licences"
date = 2023-07-21T14:32:52-04:00
draft = false
weight = 25
+++

## Le statut du logiciel 
Du point de vue de la propriété intellectuelle, quel est le status du logiciel? Est-il un produit brevetable, comme une invention ou un processus industriel, ou est-il plutôt une création qui doit être protégée par le droit d'auteur?

Le statut légal du logiciel du point de vue de la propriété intellectuelle peut varier selon les pays et leurs lois spécifiques sur la question. Généralement, il est considéré comme une création protégée par le droit d'auteur plutôt qu'un produit brevetable. Cela signifie que le code source du logiciel est protégé par les droits d'auteur, et le développeur a le droit exclusif de reproduire, distribuer et modifier son logiciel.

Cependant la question de la propriété intellectuelle dans le domaine du logiciel continue de susciter des débats passionnés entre les développeurs commerciaux et les partisans du logiciel libre. Ces derniers considèrent que le partage des connaissances et la collaboration sont essentiels pour promouvoir l'innovation et le progrès technologique. En contrepartie certains développeurs commerciaux considèrent le droit d'auteur comme un moyen de protéger leurs investissements et de rentabiliser leur travail. 

Les licences jouent un rôle essentiel dans la définition des droits et des conditions d'utilisation du logiciel, qu'il soit distribué sous une approche commerciale traditionnelle ou dans le cadre du mouvement du logiciel libre.



#### Un petite histoire
Même si on peut remonter l'histoire de la programmation informatique [assez loin](), les premiers logiciels sont apparus assez récemment, il y a quelques dizaines d'années. Avant l'invention des langages de programmation, le fait de programmer un ordinateur consistait à configurer des circuits électroniques en effectuant de nombreux branchements dans des panneaux:

![eniac](/420-313/images/Eniac.jpg?height=400px)
(Source: [Wikipedia - ENIAC](https://fr.wikipedia.org/wiki/ENIAC))

Par la suite, les langages de programmation ont permis de séparer l'aspect matériel de la configuration des ordinateurs de la tâche que ceux-ci devaient effectuer. Il s'agissait de définir une seule fois par écrit une série d'instructions, et ces instructions pouvaient ensuite être lues et exécutées par un nombre illimité de machines. Cependant, les langages étaient fortement dépendants des ordinateurs eux-mêmes: chacun avait un ensemble d'instructions différent. Ainsi, dans les années 1950, les premiers ordinateurs ont été développés pour des applications militaires, scientifiques ou administratives. À cette époque, le logiciel était considéré comme une partie intégrante du matériel informatique, et il n’existait pas de marché distinct pour le logiciel. Les fabricants d’ordinateurs fournissaient le logiciel avec le matériel, souvent sous forme de cartes perforées ou de bandes magnétiques.

Dans les années 1960, l’émergence des ordinateurs compatibles IBM permettaient d'exécuter le même logiciel indépendamment du fabricant du matériel. La **distribution** des programmes devenait possible, ce qui a permis à diverses entreprises de développer et de vendre du logiciel pour ces ordinateurs, créant ainsi le premier marché du logiciel commercial.

Dans les années 1970 et 1980, l’évolution du logiciel commercial a été marquée par l’apparition de grandes entreprises telles que Microsoft, Apple ou IBM, qui ont développé des systèmes d’exploitation et des applications pour les ordinateurs personnels. À la même époque, les premiers logiciels libres étaient développés par des programmeurs qui souhaitaient partager leur travail avec d’autres. Le mouvement du logiciel libre a pris de l’ampleur avec l’apparition d’Internet dans les années 1990, qui a facilité la collaboration entre les développeurs du monde entier. De nouvelles formes de distribution ont vu le jour (*freeware*, *shareware*, *demos*, abonnements) et différents types de licences pour les encadrer.

Aujourd'hui, le marché mondial du logiciel est suffisamment volumineux pour qu'il soit difficile de le chiffrer avec exactitude, même si [certains](https://www.statista.com/outlook/tmo/software/worldwide#revenue) estiment qu'il a engendré au-delà de $600 milliards USD en 2022. Le logiciel libre quant à lui, en particulier linux, est également omniprésent dans un nombre incalculable de téléphones, appareils domestiques ou industriels, satellites, voitures, etc.

#### Logiciel libre et 4 libertés
Le mouvement du logiciel libre s'oppose à la commercialisation des programmes informatiques en soutenant que les programmes ne sont pas un produit comme les autres. En effet, si vous possédez une voiture, il vous est impossible de la copier pour que votre voisin possède à son tour la même voiture. Lorsque vous achetez une voiture, vous payez en réalité pour quelque chose qu'il vous est impossible de faire par vous-même (car vous ne possédez ni l'expertise, ni le matériel): concevoir des pièces, les assembler, les tester, etc. 

Cependant, un logiciel se compose d'informations: il est facile de le copier pour que d'autres personnes puissent l'utiliser. Ainsi, certains le considèrent plus comme un texte écrit que comme un produit industriel. De ce point de vue, comme un texte d'opinion ou d'informations, le logiciel doit pouvoir être analysé, compris, et même amélioré par ses utilisateurs. En ce sens, le fait d'empêcher les utilisateurs de lire un programme informatique ou de le copier consiste à mettre des contraintes artificielles sur les utilisateurs, contraires à la nature même des logiciels et qui n'ont aucun autre but que d'enrichir les entreprises qui publient ces logiciels.

Le but du mouvement du logiciel libre est donc de donner la possibilité aux utilisateurs de faire ce qu'ils veulent avec les logiciels qu'ils possèdent. Ceci correspond à ce qu'on nomme les *4 libertés du logiciel libre*:
+ L'utilisateur peut utiliser le programme comme il le souhaite
+ L'utilisateur peut analyser le programme pour comprendre son fonctionnement et le modifier pour lui faire faire ce qu'il souhaite
+ L'utilisateur peut redistribuer le programme et en faire des copies aider d'autres personnes
+ L'utilisateur peut améliorer le programme et distribuer cette nouvelle version afin d'en faire bénéficier toute la communauté

{{% notice warning "Logiciel 'Open-source'" %}}
La 2e et la 4e liberté nécessite que le code source du programme soit accessible aux utilisateurs. Les programmes dont le code est ainsi disponible sont qualifiés d'**open-source**. Attention, un logiciel libre est nécessairement open-source, mais un programme open-source n'est pas nécessairement libre!
{{% /notice %}}

#### *Copyright* et *copyleft*
Pour les oeuvres écrites et les productions artistiques, les lois sur les droits d'auteurs (en anglais, "copyright law") encadrent les droits des auteurs et des éditeurs. Cependant les partisans du logiciel libre estiment que le fait d'appliquer le droit d'auteur sur les programmes informatiques est contraire aux 4 libertés. Ils ont donc proposé une alternative nommée "copyleft" qui se compose d'un ensemble de principes visant à définir un cadre légal solide autour de la notion de "logiciel libre".

Le *copyleft* inclut les 4 libertés, et aussi *l'obligation que les versions modifiées d'un logiciel libre soient elles aussi distribuées comme un logiciel libre*. Le but est d'éviter une situation où des entreprises copient un logiciel libre, y apportent des modifications et le publient ensuite comme un logiciel commercial, tirant profit du travail bénévole des programmeurs qui l'ont créé à l'origine.

{{% notice tip "Exercice" %}}
+ Qu'est-ce que la *Free Software Foundation*?
+ Qui l'a fondée et en quelle année?
+ Qu'est-ce que *GNU*, et que signifie ce nom?
{{% /notice %}}

{{% expand "Solutions" %}}
+ FSF est une organisation vouée à la diffusion du logiciel libre.
+ Richard Stallman l'a fondée en 1985.
+ "GNU" signifie "GNU is Not Unix". C'est un projet de système d'exploitation (et des programmes associés) entièrement libre et basé sur *Unix*. 
{{% /expand %}}


## Licences logicielles
Il y a donc à l'origine deux manières de distribuer les logiciels:
+ Distribution commerciale, où chaque copie doit être achetée et dont le code appartient à l'éditeur;
+ Distribution libre, où l'utilisateur peut faire ce qu'il veut avec le logiciel tant qu'il perpétue cette liberté dans ses propres versions du logiciel.

Les utilisateurs doivent être informés par l'éditeur de leurs droits en ce qui concerne l'utilisation d'un logiciel. Les **licences logicielles**, aussi appelées *CLUF* pour "Contrat de licence utilisateur final" (en anglais *EULA*, "End-User License Agreement"), sont les documents distribués en même temps qu'un logiciel et qui visent à définir le cadre dans lequel le logiciel peut être utilisé. Étant donné la nature légale de ces documents, ils sont parfois [très détaillés et très complexes](https://www.apple.com/legal/sla/docs/macOSVentura.pdf).

Si un développeur ne distribue aucune licence avec son programme, le code est automatiquement protogé par le *droit d'auteur*. Le code peut être lu et analysé par n'importe qui, *mais ne peut pas être utilisé*.

Pour connaître ses droits quant à l'utilisation d'un logiciel, l'utilisateur a besoin d'une licence. Ce sont les licences qui décrivent ce qu'on peut faire avec le logiciel. Sans licence, ces droits ne sont pas définis et les recours légaux peuvent être difficiles à justifier.

#### Licences commerciales
Le contenu des licences commerciales (qu'on appelle aussi "licences propriétaires") varie selon le fabricant et le type de logiciel, mais contiennent généralement les informations suivantes:
+ **Définition des parties**: Identification de l'éditeur du logiciel et de l'utilisateur. Permet de savoir qui est lié par les termes de la licence.
+ **Définition du logiciel**: Description du logiciel. Permet de savoir quel logiciel (nom et version) est couvert par la licence, et éventuellement quelles parties du logiciel ne sont pas couvertes.
+ **Durée de la licence**: Période de validité de la licence.
+ **Termes**: Droits et obligations de l'éditeur et de l'utilisateur. C'est ici que sont définis entre autres les utilisations permises et les restrictions, les limites reliées à la copie du logiciel, et le support offert par le fabricant. 
+ **Garanties et responsabilités**: Définit les limites de la responsabilité du fabricant (par exemple si le logiciel cause des dommages, etc.).

{{% notice tip "Exercice" %}}
Voici [la licence](https://www.ableton.com/fr/contract-de-licence-utilisateur/) du logiciel de composition musical *Ableton*. Essayez de répondre aux questions suivantes.
1. Quelle est la définition des "versions éducatives" dans le CLUF?
2. Dans quelle section est-il précisé que les utilisateurs ne peuvent pas créer un logiciel ayant la même apparence visuelle que Ableton?
3. Quelles sont les restrictions liées aux chansons de démonstration fournies avec le produit Ableton?
4. De quelle manière l'utilisateur peut-il résilier le CLUF (c'est-à-dire y mettre fin)?
{{% /notice %}}

{{% expand "Solutions" %}}
1. Les produits Ableton conçus pour une utilisation en milieu scolaire (1.6).
2. 2.3c
3. L'utilisateur ne peut pas les réutiliser ni telles quelles, ni modifiées (5.3d).
4. En supprimant ou en détruisant irréversiblement le logiciel Ableton et ses copies (16.1).
{{% /expand %}}

#### Licences libres
Il existe un [très grand nombre de licences](https://en.wikipedia.org/wiki/Category:Free_and_open-source_software_licenses) pour le logiciel libre ou *open-source* parmi lesquelles les développeurs peuvent choisir celle qui leur convient le mieux. Elles se regroupent en 2 grandes catégories: 
+ Licences *copyleft*
+ Licences *permissives*

Les licences *copyleft* se conforment [aux principes vus plus haut](/420-313/pi/licences/#copyright-et-copyleft). 

Les licences *permissives* autorisent de copier, distribuer et modifier le code tout en autorisant que des logiciels propriétaires (non libres) soient créés à partir du code.

Le tableau suivant donne les caractéristiques des 6 principales licences libres:

| Licence | Type | Caractéristiques |
|:--|:--:|:--|
| GPL (GNU General Public License) | Copyleft | La licence libre originale, entièrement conforme au *copyleft*. |
| LGPL (GNU Lesser General Public License)| Copyleft | Similaire à GPL, mais permet que le programme soit utilisé comme une librairie ou un module par d'autres programmes qui ne sont pas de licence GPL ou LGPL. |
| MPL (Mozilla Public License) | Cpoyleft "allégé" | Il est possible d'utiliser des programmes libres dans des logiciels propriétaires uniquement si les programmes libres sont dans des fichiers séparés.|
| BSD | Permissive | Inspirée de la licence du système d'exploitation *Unix* "Berkeley Software Distribution". Pas de restrictions sur la distribution, il suffit de nommer le développeur original. La licence inclut une clause de non-responsabilité du développeur original sur les logiciels dérivés. | 
| MIT | Permissive | Créée au *Massachusetts Institute of Technology*.Très similaire à la licence BSD. La licence MIT est cependant plus courte et inclut la documentation sur le logiciel. | 
| Apache | Permissive | Créée par la *Apache Software Foundation*. | Les programmes dérivés peuvent l'être sous une licence propriétaire mais doivent inclure l'information qu'ils ont été modifiés.|

{{% notice tip "Exercice" %}}
Pour chacun des logiciels suivants, sous quelle licence est-il distribué?
1. Adminer
2. VLC Media Player
3. LibreOffice
4. Putty
5. React
6. Le noyau linux
7. 7-Zip
{{% /notice %}}

{{% expand "Solutions" %}}
1. Apache / GPL 
2. GPL
3. MPL
4. MIT
5. MIT
6. GPL
7. LGPL
{{% /expand %}}



## À consulter
+ [La révolution du logiciel libre](https://www.youtube.com/watch?v=NTuJHcEoHLs)
+ [SOFTWARE LICENSES - Proprietary Software, Free and Open Source Software FOSS, and Public Domain (en anglais)](https://www.youtube.com/watch?v=UFLvqgwvh4Q)
  
## Références
+ [Droits de propriété intellectuelle sur les logiciels au Canada](https://ised-isde.canada.ca/site/office-propriete-intellectuelle-canada/fr/droits-propriete-intellectuelle-logiciels-canada)
+ [Logiciel libre (Wikipedia)](https://en.wikipedia.org/wiki/Free_software)
+ [Clauses types d'une licence (Gouv. Canada)](https://www.canada.ca/fr/reseau-information-patrimoine/services/propriete-intellectuelle-droit-auteur/guide-elaboration-strategie-contrats-licence-contenu-numerique/clauses-types-licence.html)
+ [Choose an open-Source License](https://choosealicense.com/licenses/)
+ [Types de licences *open-source* (Snyk)](https://snyk.io/learn/open-source-licenses/)