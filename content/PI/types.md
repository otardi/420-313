+++
pre = "<b>2.1 </b>"
title = "Qu'est-ce que la propriété intellectuelle?"
date = 2023-07-21T14:31:51-04:00
draft = false
weight = 21
+++

> Le terme “propriété intellectuelle” désigne les œuvres de l’esprit : inventions; œuvres littéraires et artistiques; dessins et modèles; et emblèmes, noms et images utilisés dans le commerce.

*Organisation Mondiale de la Propriété Intellectuelle* (https://www.wipo.int/about-ip/fr/)

La PI se divise en deux: **propriété artistique** (les droits d'auteur) et **propriété industrielle** (qui comprend les brevets, secrets et marques).

## Droits d'auteur
#### Principe
Une personne qui crée une oeuvre, comme un peintre, un écrivain ou un musicien, a droit à une partie des revenus générés par son oeuvre. Ces droits sont limités dans le temps: après cette durée, l'oeuvre passe au domaine public. La durée correspond généralement à la vie de l'auteur et peut être prolongée pour ses héritiers.

#### Histoire
À la fin du Moyen-Âge, l'invention de l'imprimerie permit de copier et de diffuser facilement les oeuvres littéraires. Il n'y avait cependant pas de lois bien établies qui déterminaient comment les auteurs étaient payés. Les éditeurs achetaient parfois les oeuvres aux auteurs et en récoltaient tous les revenus, parfois après les avoir modifiées eux-mêmes.

Dans différents pays d'Europe, on commença à la Renaissance à formuler des lois qui donnaient aux auteurs des droits exclusifs d'exploitation sur leurs écrits, ce qui leur permit non seulement d'être payés plus justement pour leur travail, mais aussi de contrôler les modifications faites à leurs oeuvres. Ces deux éléments sont à la base de de toutes les lois qui protègent encore aujourd'hui la propriété intellectuelle.

À l'ère industrielle, cinéma, musique, et plus tard la télévision sont à l'origine de productions artistiques de grande envergure, et les lois encadrant le droit d'auteur devinrent plus nombreuses afin d'encadrer les différents types de production, de méthodes de diffusion, etc. Aussi la notion d'*auteur* s'est raffinée, incluant différents types de contributions: par exemple les auteurs d'une chanson peuvent inclure le compositeur, les paroliers et le producteur; les auteurs d'un film peuvent inclure les producteurs, le réalisateur et les scénaristes. Il peut aussi exister différents types d'ententes, comme par exemple dans le milieu du cinéma des USA où les scénaristes sont salariés et les compagnies de production conservent les droits (ententes que l'on considère parfois injustes, [comme en témoignent des évènements récents](https://fr.wikipedia.org/wiki/Gr%C3%A8ve_de_la_Writers_Guild_of_America_de_2023)).

#### Partage/piratage
À la fin des années 1990, la croissance du web facilite la transmission de contenus de toutes sortes. Le protocole *BitTorrent*, des applications comme *Napster*, *LimeWire* et *Kazaa* et le site *PirateBay* voient le jour et il devient facile de copier une oeuvre et de la rendre disponible à des millions de personnes. Les compagnies de production se défendent (mais pas tant les auteurs, [à part quelques exceptions](https://www.lemonde.fr/big-browser/article/2012/12/07/hug-metallica-se-reconcilie-avec-napster_5986416_4832693.html)) et le piratage devient une position politique car selon plusieurs elle ne sert plus à défendre le droits des auteurs eux-mêmes mais plutôt celui des compagnies multi-millionnaires qui profitent de leur travail. 

{{% notice tip "Exercice" %}}
Quels artistes ou groupes de musique sont favorables au partage de leur musique? Quelles sont leurs raisons? Faites une recherche pour en trouver 4 et notez vos références (au moins 2 pour chaque artiste).
{{% /notice %}}

## Brevets
#### Principe
Un inventeur a l'exclusivité d'exploiter son invention, c'est-à-dire développer et vendre un produit basé sur cette invention durant une certaine période (généralement 20 ans), après laquelle l'invention passe au domaine public. Le but est de donner un avantage temporaire à l'inventeur en évitant qu'il soit en concurrence avec d'autres entreprises. 

#### Histoire
Au début du 15e siècle, Venise est un centre industriel et commercial important. On y voit apparaître les premières inventions "protégées" par des privilèges d'exploitation, mais il n'existe pas encore de lois pour encadrer ce privilège; la première apparaîtra cependant en 1474 au nom de *Parte Veneziana*. Elle jette les bases des lois qui suivront dans de nombreux pays d'Europe aux 16e et 17e siècles.

La révolution industrielle du 19e siècle voit de nombreuses innovations apparaître et les pays se dotent d'organisations officielles pour la gestion des brevets, dont les demandes se font de plus en plus nombreuses. Au 20e siècle, de nouveaux secteurs s'ajoutent à ce qui peut constituer une invention: la biotechnologie, notamment pour ce qui touche aux inventions en agriculture comme les semences et les engrais, et la chimie. Des accords internationaux ont également vu le jour pour harmoniser les lois sur les brevets entre les pays du monde.

Avec l'avènement de l'informatique, le domaine des logiciels se présente comme pouvant à son tour faire l'objet de brevets, mais dans ce cas-ci les lois varient entre les pays. Dans certains pays on met des limites aux types de logiciels qui peuvent être brevetés, voire on exclut carrément les programmes informatiques de ce qui être être brevetable. La brevetabilité des logiciels fait toujours l'objet d'un débat.

#### Les critères d'une invention brevetable
Pour pouvoir être brevetée, une invention doit satisfaire 3 critères:
+ Être nouvelle: il ne doit pas exister déjà une invention semblable
+ Être utile: elle doit constituer une solution technique à un problème technique
+ Être inventive: elle ne doit pas être évidente pour une personne qui connaît le domaine

Lorsqu'on dépose une demande de brevet, celle-ci est analysée afin de bien vérifier si l'invention répond à ces 3 critères.

{{% notice tip "Exercice" %}}
Les inventions suivantes peuvent-elles faire l'objet d'un brevet? Si non, pourquoi?
1. Une méthode d'emballage alimentaire qui prolonge la période de conservation des aliments.
2. Une formule chimique pour un une substance qui améliore l'imperméabilité des tissus.
3. Une pelle pour gauchers.
4. Une technique permettant de désaliniser l'eau de mer pour la rendre potable.
5. Une technique de méditation pour améliorer la relaxation.
{{% /notice %}}

{{% expand "Solutions" %}}
1. Oui
2. Oui
3. Non: une pelle ordinaire est autant utilisable pour les gauchers que le droitiers, donc il n'existe pas de problème technique dont l'invention serait la solution.
4. Oui
5. Non: la relaxation n'est pas un problème technique et n'a pas réellement d'application industrielle.
{{% /expand %}}

#### Le contenu d'un brevet
Contrairement à une production artistique, qui est protégée au moment-même de sa création, pour breveter une invention il faut faire une demande officielle et cette demande doit décrire adéquatement l'invention. Elle se compose de deux grandes sections:
+ La **description**, qui donne les détails de fonctionnement de l'invention. Elle explique le domaine technologique de l'invnetion, son utilité, les inventions existantes qui lui resseblent et inclut souvent des dessins techniques ou des diagrammes;
+ Les **revendications**, qui expliquent pourquoi l'auteur considère que son invention doit faire l'objet d'un brevet, et de quelle manière elle satisfait les 3 critères cités plus haut.

> Un exemple: la motoneige, [brevetée par J.-A. Bombardier en 1937](/420-313/367104.pdf).

#### Les exceptions: tout n'est pas brevetable
On ne peut pas breveter n'importe quoi, évidemment: pour des raisons légales ou éthiques, la loi interdit de breveter certaines choses. L'objectif est de protéger les vraies inventions, pas de limiter l'utilisation de celles-ci. 

Ainsi il est impossible de déposer une demande de brevet pour les choses suivantes:
+ Une idée ou un concept
+ Une théorie scientifique
+ Une méthode de traitement médical
+ Des formes de vie supérieures (par exemple un être vivant modifié génétiquement)
+ Des formes d'énergie
+ Des inventions non-techniques ou esthétiques
+ Des imprimés
  
#### Les abus: un frein à l'innovation
De nombreuses personnes ou compagnies voient dans les brevets une manière de s'enrichir. En effet, si on possède une centaine de brevets dans un domaine en pleine croissance, il est très possible qu'une invention apparaisse et qu'elle soit suffisamment proche d'un de nos brevets pour qu'il soit possible de lui réclamer des droits. 

Ainsi on a vu des entreprises, après avoir inventé un produit ou une nouvelle technologie, déposer des brevets pour différents usages de cette technologie sans créer les inventions associées à ces brevets. Par exemple, on invente un produit nommé "Machin" et on dépose en même temps un brevet pour d'autres produits: un chargeur de Machin, un porte-Machin pour l'auto, un bidule pour utiliser le Machin sous l'eau, etc. On ne souhaite pas développer ces produits mais si jamais une autre compagnie le fait, on pourra lui réclamer des droits.

Aussi, puisque les procès dans le domaine des brevets sont généralement très longs et coûteux, posséder des brevets peut s'avérer dissuasif. Si par exemple vous inventez un produit complémentaire à l'utilisation du iPhone et que vous constatez qu'il existe un produit assez proche (mais pas identique) breveté par Apple mais jamais commercialisé, seriez-vous prêt à vous lancer dans une bataille juridique des plusieurs centaines de milliers de dollars avec eux? Comme on le constate le simple fait de posséder des brevets peut éliminer la concurrence.

Ces deux exemples montrent assez bien comment les brevets peuvent dans certains cas être des limites à l'innovation. 

{{% notice tip "Exercice" %}}
Voici un cas historique de procès entre deux géants de l'informatique: [Google v. Oracle](https://www.lemondeinformatique.fr/actualites/lire-java-et-android-google-gagne-son-litige-contre-oracle-82508.html)

À partir de ce texte (ou d'autres articles sur le même sujet, au besoin), répondez aux questions suivantes:
1. Sur quel produit portait la réclamation? 
2. Qui poursuivait qui? 
3. Combien de dommages étient demandés?
4. Qui a remporté la cause?
{{% /notice %}}

{{% expand "Solutions" %}}
1. Le langage Java, plus particulièrement des API de Java utilisées dans *Android*
2. Oracle poursuivait Google
3. 9 milliards USD
4. Google
{{% /expand %}}

## Marques 
En plus de protéger leurs inventions, les entreprises ont aussi des droits relativement à l'utilisation de leur *image de marque* et tout ce qui s'y rattache. Le but de ces droits est de protéger l'identité et la réputation de la compagnie de ceux qui veulent s'en servir à leur compte, soit pour ternir cette image ou encore pour profiter de cette réputation. Les logos, slogans, noms de produits (qu'on nomme *marque de commerce* ou "trademark") sont protégés par ces droits. Les symboles ® et ™ aux USA (ou "MD" et "MC" en français au Canada) servent à désigner les marques ainsi protégées.

Au Canada et aux USA, une marque de commerce est enregistrée pour 10 ans; durant cette période, il est interdit à d'autres de l'utiliser, ou encore d'utiliser un nom ou une image qui lui ressemblerait assez pour porter à confusion.

{{% notice tip "Exercice" %}}
Quelle est la différence entre ® et ™ du point de vue légal?
{{% /notice %}}

{{% expand "Solutions" %}}
Le symbole ® indique une marque déposée au Registre des marques alors que ™ indique une marque non déposée (https://fr.wikipedia.org/wiki/Symbole_de_marque_d%C3%A9pos%C3%A9e).
{{% /expand %}}

## Secrets industriels
Pour diverses raisons, il arrive que les entreprises préfèrent ne pas breveter certains de leurs produits et choisissent plutôt d'avoir recours au secret. En effet, le brevet lui-même est public: l'invention est protégée pour 20 ans, mais son fonctionnement est décrit dans le brevet, et ceux-ci sont accessibles au public. Certaines entreprises (notamment dans le domaine militaire) ne souhaitent pas que ces informations soient connues, ou encore qu'elles ne leur appartiennent plus après 20 ans. La [recette de Coca-Cola](https://en.wikipedia.org/wiki/Coca-Cola_formula) en est un bon exemple.

Aussi, puisqu'il existe encore des débats sur la possibilité de breveter des logiciels ou des algorithmes, plusieurs entreprises traitent leur code comme des secrets industriels: par exemple, les algorithmes qui déterminent ce qui apparaît dans les fils Facebook ou Instagram sont très protégés. 

Pour ces deux raisons, de nombreuses attaques informatiques visent ce type de propriété intellectuelle.

Du point de vue légal, un secret industriel est protégé seulement à condition que le détenteur de ce secret démontre qu'il a fait les efforts nécessaires pour le protéger. Et puisqu'il n'existe pas de manière officielle d'enregistrer un secret industriel, les recours légaux, si les secrets sont révélés, peuvent être très complexes.


-------------------
## À consulter
+ [Droit d'auteur: les bases (Ligue des auteurs professionnels)](https://www.youtube.com/watch?v=EBMdkjAsYfM)
+ [Qu'est-ce que la propriété intellectuelle? (Gouv. Canada)](https://www.youtube.com/watch?v=axL7JY5mC90)
+ [Qu'est-ce qu'un brevet? (Gouv. Canada)](https://www.youtube.com/watch?v=0fK0aF4-gwM)



## Référence
+ [Qu'est-ce qu'un droit d'auteur? (Gouv. du Canada)](https://ised-isde.canada.ca/site/office-propriete-intellectuelle-canada/fr/quest-propriete-intellectuelle/quest-droit-dauteur)
+ [Protéger votre innovation (Gouv. du Canada)](https://ised-isde.canada.ca/site/office-propriete-intellectuelle-canada/fr/quest-propriete-intellectuelle/protegez-votre-innovation)
+ [Lapointe, Serge (2000), "L'histoire des brevets", *Les cahiers de la propriété intellectuelle*, volume 12, Numéro 13.](https://cpi.openum.ca/articles/v12/n3/lhistoire-des-brevets/)

