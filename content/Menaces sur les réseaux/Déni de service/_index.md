+++
chapter = true
pre = "<b>5.1 </b>"
title = "Attaques par Déni de Service (DoS)"
draft = false
weight = 51
+++

Les attaques par déni de service (DoS) sont un fléau omniprésent dans le monde numérique d'aujourd'hui. Elles représentent une menace constante pour les entreprises, les sites web, les réseaux et même les individus. Dans cette section, nous allons explorer en détail les attaques par déni de service, leurs différentes formes, les conséquences potentiellement dévastatrices et les stratégies pour se protéger et réagir en cas d'attaque.

## Comprendre les Attaques par Déni de Service

Les attaques par déni de service, communément appelées DoS, sont des tentatives malveillantes visant à perturber le fonctionnement normal d'un service en inondant le système cible de trafic indésirable ou en exploitant des vulnérabilités pour l'empêcher de répondre aux demandes légitimes. Voici un aperçu des éléments clés de ces attaques :

### Mécanismes de Base

1. **Inondation de Trafic** : Les attaques DoS consistent souvent à submerger une cible avec une quantité excessive de trafic, ce qui sature la bande passante, la capacité CPU ou la mémoire du système et le rend inutilisable.

2. **Épuisement des Ressources** : Certaines attaques DoS ciblent spécifiquement les ressources du système, telles que la mémoire ou les connexions réseau, pour les épuiser progressivement.

3. **Exploitation de Vulnérabilités** : Les attaquants peuvent cibler des failles de sécurité connues pour provoquer des pannes ou des ralentissements.

### Types d'Attaques DoS

1. **Attaques DoS Classiques** : Ces attaques impliquent généralement un seul attaquant qui tente de saturer la cible avec du trafic malveillant.

2. **Attaques par Déni de Service Distribué (DDoS)** : Ces attaques mobilisent un grand nombre de dispositifs compromis, créant ainsi une armée de "zombies" pour attaquer la cible simultanément, chacun avec une adresse IP différente.

3. **Attaques d'Amplification** : Les attaques d'amplification exploitent des serveurs vulnérables pour multiplier le trafic envoyé à la cible.

## Les Conséquences des Attaques par Déni de Service

Les attaques DoS peuvent avoir un impact dévastateur sur les organisations et les individus. Voici quelques-unes des conséquences potentielles :

### Perturbation des Services

L'objectif principal des attaques DoS est de perturber les services en ligne, ce qui peut entraîner :

- **Indisponibilité du Service** : Les utilisateurs légitimes ne peuvent plus accéder aux services, ce qui peut nuire à la réputation de l'entreprise et entraîner des pertes financières.

- **Ralentissements** : Même si les services restent disponibles, leur performance peut être considérablement réduite, provoquant des ralentissements et des temps de réponse plus longs.

### Coûts Élevés

La gestion et la résolution des attaques DoS peuvent s'avérer coûteuses pour les organisations. Les coûts peuvent inclure :

- **Investissements dans la Sécurité** : Les entreprises doivent investir dans des mesures de sécurité supplémentaires pour se protéger contre de futures attaques.

- **Perte de Revenus** : Les périodes d'indisponibilité des services peuvent entraîner une perte de revenus, en particulier pour les entreprises axées sur le commerce en ligne.

### Impact sur la Réputation

Les attaques DoS peuvent ternir la réputation d'une entreprise. Les utilisateurs mécontents peuvent se tourner vers des concurrents, et la confiance des clients peut être ébranlée.

## Prévention des Attaques par Déni de Service

La prévention est essentielle pour minimiser les risques liés aux attaques DoS. Voici quelques stratégies de prévention :

### 1. Pare-feu et Filtrage du Trafic

L'utilisation de pare-feu et de systèmes de filtrage du trafic peut aider à bloquer le trafic malveillant avant qu'il n'atteigne le système cible.

### 2. Équilibrage de Charge

L'équilibrage de charge distribue le trafic entre plusieurs serveurs, réduisant ainsi la vulnérabilité aux attaques DoS en répartissant la charge.

### 3. Surveillance du Trafic

Une surveillance constante du trafic réseau permet de détecter les signes précurseurs d'une attaque DoS imminente, permettant ainsi une réponse rapide.

### 4. Plan de Continuité d'Activité (PCA)

Un PCA bien élaboré permet à une organisation de maintenir ses opérations même en cas d'attaque DoS en cours.

## Réagir aux Attaques par Déni de Service

En cas d'attaque DoS, une réponse rapide et appropriée est cruciale. Voici les étapes à suivre :

### 1. Identifier l'Attaque

Il est essentiel de reconnaître rapidement qu'une attaque est en cours en surveillant les signes de perturbation du service.

### 2. Isoler le Trafic Malveillant

Les outils de sécurité doivent être utilisés pour isoler le trafic malveillant et rediriger le trafic légitime.

### 3. Mitigation

Appliquer des mesures de mitigation, telles que le filtrage du trafic ou le blocage des adresses IP malveillantes, pour minimiser les dommages.

### 4. Communication

Informer les parties prenantes, y compris les utilisateurs, des perturbations et des mesures prises pour y remédier.


## Exemples de vulnérabilités aux DoS

Si les utilisateurs peuvent fournir, directement ou indirectement, une valeur qui spécifie le nombre d'objets à créer sur le serveur d'application, et si le serveur n'impose pas de limite supérieure stricte à cette valeur, il est possible d'amener l'environnement à manquer de mémoire disponible. Le serveur peut commencer à allouer le nombre requis d'objets spécifiés, mais s'il s'agit d'un nombre extrêmement élevé, cela peut entraîner de graves problèmes sur le serveur, en remplissant éventuellement toute la mémoire disponible et en altérant ses performances.

##DoS de l'entrée utilisateur en tant que compteur de boucle

Comme pour le problème précédent d'allocation d'objets spécifiés par l'utilisateur, si l'utilisateur peut directement ou indirectement assigner une valeur qui sera utilisée comme compteur dans une fonction en boucle, cela peut causer des problèmes de performance sur le serveur.

Voici un exemple de code vulnérable en Java :

```java
public class MyServlet extends ActionServlet {
   public void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
          . . .
          String [] values = request.getParameterValues("CheckboxField") ;
      // Traite les données sans vérifier la longueur pour un intervalle raisonnable - c'est mauvais !
          for ( int i=0 ; i<values.length ; i++) {
                // beaucoup de logique pour traiter la demande
         }
         . . .
   }
    . . .
}
```

Comme nous pouvons le voir dans cet exemple simple, l'utilisateur a le contrôle sur le compteur de la boucle. Si le code à l'intérieur de la boucle est très exigeant en termes de ressources et qu'un attaquant le force à être exécuté un très grand nombre de fois, cela peut diminuer les performances du serveur dans le traitement d'autres requêtes, provoquant une condition DoS.

##Défaut de libération des ressources dans le cadre d'un déni de service

Si une erreur se produit dans l'application et empêche la libération d'une ressource en cours d'utilisation, celle-ci peut devenir indisponible. Voici quelques exemples possibles :

Une application verrouille un fichier pour l'écriture, puis une exception se produit sans que le fichier soit explicitement fermé et déverrouillé.

Fuite de mémoire dans les langages où le développeur est responsable de la gestion de la mémoire, tels que C et C++. 

Dans le cas où une erreur entraîne le contournement du flux logique normal, la mémoire allouée peut ne pas être supprimée et peut être laissée dans un état tel que le ramasse-miettes ne sait pas qu'elle doit être récupérée.

Utilisation d'objets de connexion à la base de données qui ne sont pas libérés en cas d'exception. Un certain nombre de requêtes répétées de ce type peuvent amener l'application à consommer toutes les connexions à la base de données, car le code conserve l'objet ouvert de la base de données, sans jamais libérer la ressource.

Voici un exemple de code vulnérable en Java. Dans cet exemple, la connexion et le CallableStatement doivent être fermés dans un bloc final.

```java
public class AccountDAO {
    ... ...
    public void createAccount(AccountInfo acct)
                 throws AcctCreationException {
       ... ...
           try {
            Connection conn = DAOFactory.getConnection() ;
            CallableStatement calStmt = conn.prepareCall(...) ;
          ... ... 
           calStmt.executeUpdate() ;
           calStmt.close() ;
          conn.close() ;
       } catch (java.sql.SQLException e) {
            lance AcctCreationException (...) ;
       }
    }
}
```

Débordements de mémoire tampon DoS

Tout langage dans lequel le développeur est directement responsable de la gestion de l'allocation de la mémoire, notamment les langages C et C++, présente un risque de débordement de la mémoire tampon. Si le risque le plus sérieux lié à un dépassement de mémoire tampon est la possibilité d'exécuter un code arbitraire sur le serveur, le premier risque provient du déni de service qui peut se produire si l'application se bloque.

Voici un exemple simplifié de code vulnérable en C :

```C
void overflow (char *str) {
   char buffer[10] ;
   strcpy(buffer, str) ; // Dangereux !
}

int main () {
  char *str = "Ceci est une chaîne de caractères plus grande que le tampon de 10" ;
  overflow(str) ;
}
```

Si cet exemple de code était exécuté, il provoquerait une erreur de segmentation et un vidage du noyau (core dump). En effet, strcpy essaierait de copier 53 caractères dans un tableau de 10 éléments seulement, en écrasant les emplacements de mémoire adjacents. Bien que l'exemple ci-dessus soit extrêmement simple, la réalité est que dans une application web, il peut y avoir des endroits où la longueur des données saisies par l'utilisateur n'est pas vérifiée de manière adéquate, ce qui rend ce type d'attaque possible.

DoS Stockage d'une trop grande quantité de données dans la session

Il faut veiller à ne pas stocker trop de données dans un objet de session utilisateur. Le stockage d'une trop grande quantité d'informations dans la session, comme de grandes quantités de données extraites de la base de données, peut entraîner des problèmes de déni de service. Ce problème est exacerbé si les données de session sont également suivies avant une connexion, car un utilisateur peut lancer l'attaque sans avoir besoin d'un compte.

## Conclusion

Les attaques par déni de service sont une menace constante dans le monde numérique. Comprendre ces attaques, leurs conséquences et les moyens de prévention est essentiel pour protéger les systèmes et les données. En adoptant des pratiques de sécurité solides et en étant prêt à réagir en cas d'attaque, les organisations peuvent minimiser leurs impacts.

## Questions/réponses

1. **Qu'est-ce qu'une attaque par déni de service (DoS) ?**

   Une attaque par déni de service (DoS) est une tentative malveillante visant à perturber le fonctionnement normal d'un système, d'un service ou d'un réseau en inondant la cible de trafic indésirable ou en exploitant des vulnérabilités pour la rendre indisponible.

2. **Quelle est la différence entre une attaque DoS et une attaque DDoS ?**

   Une attaque DoS est menée par un seul attaquant, tandis qu'une attaque DDoS (Attaque par Déni de Service Distribué) mobilise un grand nombre de dispositifs compromis pour attaquer la cible simultanément, rendant l'attaque plus puissante.

3. **Quelles sont les motivations derrière les attaques DoS ?**

   Les motivations peuvent varier, mais elles incluent souvent la rivalité commerciale, la revendication politique, le sabotage ou simplement le désir de causer des perturbations et des dégâts.

4. **Comment fonctionne une attaque DDoS ?**

   Dans une attaque DDoS, des ordinateurs zombies ou des dispositifs compromis envoient une grande quantité de trafic à la cible, ce qui la submerge et la rend indisponible.

5. **Quels sont les signes d'une attaque DoS en cours ?**

   Les signes incluent une diminution des performances du réseau ou des services, des temps de réponse inhabituellement longs, des erreurs de connexion fréquentes et une utilisation anormale de la bande passante.

6. **Comment peut-on se protéger contre les attaques DoS ?**

   Les mesures de protection incluent l'utilisation de pare-feu, de systèmes de détection d'intrusion, de services CDN, et la mise en place de plans de continuité d'activité.

7. **Les attaques DoS sont-elles illégales ?**

   Oui, les attaques DoS sont illégales dans la plupart des juridictions car elles causent des perturbations et des dommages aux systèmes, aux entreprises et aux individus.

8. **Peut-on tracer l'origine d'une attaque DDoS ?**

   L'origine d'une attaque DDoS peut être difficile à tracer en raison de la diversité des dispositifs utilisés, mais des enquêtes forensiques et des analyses de trafic peuvent aider à identifier les coupables.

9. **Existe-t-il des outils de prévention des attaques DoS ?**

   Oui, il existe des outils de prévention, tels que les systèmes de détection d'intrusion (IDS), les pare-feu avancés, les services CDN (Content Delivery Network) et les filtres de trafic, qui peuvent aider à atténuer les attaques DoS.

10. **Comment réagir en cas d'attaque DoS en cours ?**

    En cas d'attaque DoS, il est important de suivre un plan de continuité d'activité, d'isoler le trafic malveillant, d'appliquer des mesures de mitigation et de communiquer avec les parties prenantes pour minimiser les perturbations et les dommages.

## Vidéo

![Comprendre l'attaque DDOS en 4 minutes](https://youtu.be/n4Zs0qcgjXI?si=GmGCRf3kwtIALLKh)