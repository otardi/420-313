+++
chapter = true
pre = "<b>3.1 </b>"
title = "Cryptographie classique"
date = 2023-08-05T13:49:26-04:00
draft = false
weight = 31
+++

La cryptographie n’est pas une science récente : certains documents historiques montrent [qu’on utilisait des systèmes d’encodage il y a 2500 ans](https://fr.wikipedia.org/wiki/Histoire_de_la_cryptologie). La cryptographie, à la base, a pour objectif que seuls les destinataires d’un message soient en mesure d’en lire le contenu. Le principe est simple :
+ Celui qui rédige le message transforme son contenu en utilisant une méthode spécifique, le rendant ainsi incompréhensible;
+ Le message ainsi codé (on dit aussi : chiffré) est envoyé au destinataire;
+ Le destinataire utilise la même méthode pour déchiffrer le message.
  
On appelle **chiffre** cette méthode utilisée pour chiffrer et déchiffrer un message.

## Chiffre de César
Un exemple que tout le monde connaît est le *chiffre de César*, ainsi nommé car il était utilisé par Jules César pour communiquer avec ses armées lorsque ses messages contenaient des informations sensibles. Cette méthode consiste simplement à décaler les lettres de l’alphabet d’un nombre donné de caractères : par exemple, si le décalage vaut 3, alors `A` est remplacé par `D`, `B` par `E`, `C` par `F`, etc. comme suit:

![cesar](/420-313/images/cesar.png)

Avec le chiffre de César, si quelqu’un qui intercepte le message connaît la méthode de chiffrement, il aura tout de même besoin d’une information supplémentaire pour déchiffrer le message, soit la valeur de décalage utilisée. On appelle clé cette information essentielle à l’interprétation du chiffre. Évidemment, si on sait qu’un message est codé grâce au chiffre de César, il est assez simple de le déchiffrer même sans disposer de la clé car il n’existe que 26 clés possibles.

{{% notice tip "Exercice" %}}
1. Chiffrez la phrase suivante avec le code de César et la clé **-5**: "Le chat attrape la souris".
2. Déchiffrez le texte suivant sachant que la clé vaut **13**: "Yr irag fbhssyr qbhprzrag pr fbve".
3. Quelle est le texte en clair pour le message suivant chiffré avec le chiffre de César? "Dwn ounda nluxc nw brunwln"
{{% /notice %}}

{{% expand "Solutions" %}}
1. "Gz xcvo voomvkz gv njpmdn"
2. Le vent souffle doucement ce soir
3. Une fleur éclot en silence
{{% /expand %}}

## Chiffre de Vigenère
Durant environ 300 ans (du 16e au 19e siècle), le *chiffre de Vigenère* a eu la réputation d'être indéchiffrable, c’est-à-dire qu’il n’existait aucune méthode pour décrypter les messages à moins d’avoir la clé, et même si on savait que le message avait été chiffré avec la méthode de Vigenère.

Le chiffre de Vigenère est une technique de substitution polyalphabétique, c'est-à-dire que plusieurs alphabets (en réalité, plusieurs valeurs de décalage) sont utilisées pour chiffrer et déchiffrer les messages. Son fonctionnement est similaire au chiffre de César où on utiliserait plus d'une valeur de décalage. 

#### Méthode
La méthode est simple: la clé est un mot dont la position de chacune des lettres est une valeur de décalage. Par exemple, si la clé est le mot `ABRI`, cela correspond aux valeurs `0-1-17-8`.

Pour chiffrer un message, par exemple "Le ciel est bleu", on applique la clé sur ce message (en la répétant au besoin):
| | | | | | | | | | | | | | |
|:--|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|Original         | L | E | C  | I | E | L | E  | S | T | B | L  | E | U |
|Clé              | A | B | R  | I | A | B | R  | I | A | B | R  | I | A |
|Décalage         | 0 | 1 | 17 | 8 | 0 | 1 | 17 | 8 | 0 | 1 | 17 | 8 | 0 |
|Message chiffré  | L | F | T  | Q | E | M | V  | A | T | C | C  | M | U |

Pour déchiffrer le message, il suffit d'appliquer le clé sur le message chiffré et décaler les lettres dans le sens inverse. Une manière un peu plus rapide de chiffrer et de déchiffrer les messages ainsi chiffrés consiste à utiliser une table comme la suivante:

![tablevig](/420-313/images/tablevig.png?width=550px)

Dans cette table, à des fins de clarté, on montre uniquement le chiffrement / déchiffrement des 4 premières lettres du message, `LECI`.

{{% notice tip "Exercice" %}}
Vous recevez le message suivant chiffré par la méthode de Vigenère:
+ Texte chiffré: `yshjvykdqdyihfopexrabmrmgdmjjmozqnvrgzqdfypvqsuifcxfzp`.
+ Clé: `nombre`
  
Quel est le texte en clair?
{{% /notice %}}

{{% expand "Solutions" %}}
"Le vieux pêcheur contemplait paisiblement le coucher de soleil"
{{% /expand %}}

## *Enigma*
Durant la seconde guerre mondiale, l’armée nazie utilisait un système cryptographique nommé **Enigma** pour chiffrer les messages transmis entre ses différentes divisions. 

![enigma](/420-313/images/enigma.jpg?width=450px)

Utilisé correctement, ce système aurait pu permettre une confidentialité totale: des chercheurs de l’époque considéraient en effet que les messages chiffrés avec Enigma étaient impossibles à déchiffrer. Mais grâce à un espion, les alliés ont eu accès à des documents techniques qui ont peu à peu permis d'en comprendre le fonctionnement. D'importants travaux ont été réalisés pour mettre sur pied un système efficace de décryptage d'Enigma; la machine servant à effectuer ce décryptage, *Colossus*, peut être considérée comme un des premiers ordinateurs. 

Pour chiffrer un message avec Enigma, il fallait entrer une clé (de 3 ou 4 caractères selon le modèle) au moyen de rotors, puis taper chaque lettre du message à l'aide d'un clavier. Pour chaque lettre du message original ainsi entrée, la lettre du message chiffré s'allumait. Pour déchiffrer un message, on n'avait qu'à taper les lettres du message chiffré. Afin de rendre difficile le déchiffrement des messages, la clé changeait chaque jour.

## Enjeux en cryptographie
Dans le code de César, le chiffre de Vigenère et Enigma, tout comme dans les systèmes cryptographiques contemporains, on retrouve les éléments suivants:
+ Le message en clair: (en anglais, "cleartext") ce qu'on veut chiffrer
+ Le message chiffré: (en anglais, "ciphertext") le résultat du chiffrement
+ Le chiffre: (en anglais, "cipher") la méthode utilisée pour chiffrer
+ La clé: l'information secrète qu'on utilise pour chiffrer et déchiffrer les messages

Le but d’un système cryptographique est de rendre très difficile (idéalement, impossible) de dériver le message en clair à partir du message chiffré si on ne possède pas la clé. Pour bien des systèmes cryptographiques, [il est moins difficile de se procurer la clé que de déchiffrer le message](https://xkcd.com/538). Un problème se pose alors : celui de s’assurer que la clé demeure secrète.

Même si on utilise des chiffres de plus en plus complexes, qui prendraient en théorie 300 ans à déchiffrer avec les ordinateurs les plus puissants, le code ne sert plus à rien si la clé tombe entre les mains de ceux à qui elle n'est pas destinée. Ainsi pendant des centaines et des centaines d'années, de Jules César à la machine Enigma utilisée durant la seconde guerre mondiale, le problème de la confidentialité de la clé est demeuré central. Jusqu'à 1976, où deux mathématiciens ont eu une idée révolutionnaire.

## À consulter
+ [Chiffre de César sur *dCode*](https://www.dcode.fr/chiffre-cesar)
+ [Chiffre de Vigenère sur *dCode*](https://www.dcode.fr/chiffre-vigenere)
+ [Enigma sur *dCode*](https://www.dcode.fr/chiffre-machine-enigma)
+ [Décrypter Vigenère](https://www.youtube.com/watch?v=uxFqkB_Mf-U)
+ [Fonctionnement détaillé d'Enigma](https://www.youtube.com/watch?v=ybkkiGtJmkM)

## Références
+ [Enigma (Wikipedia)](https://fr.wikipedia.org/wiki/Enigma_(machine))
  