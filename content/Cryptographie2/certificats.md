+++
chapter = true
pre = "<b>4.2</b>"
title = "Certificats numériques"
date = 2023-09-14T08:28:44-04:00
draft = false
weight = 42
+++

TLS (Transport Layer Security) est le protocole de chiffrement utilisé dans les échanges HTTPS. Dans ce protocole, on utilise à la fois le chiffrement symétrique et asymétrique. Le principe est le suivant :

1. Alice veut envoyer un message à Bob. Elle doit :
   - Générer une clé symétrique 
   - Chiffrer le message avec cette clé
   - Utiliser la clé publique de Bob pour chiffrer la clé symétrique
   - Envoyer le message chiffré et la clé chiffrée à Bob

2. Bob veut lire le message d'Alice. Il doit :
   - Utiliser sa clé privée pour déchiffrer la clé symétrique
   - Utiliser la clé symétrique pour déchiffrer le message

Sur un site HTTPS, lorsqu’on souhaite envoyer des données confidentielles (comme par exemple un numéro de carte de crédit), on doit donc avoir en notre possession la clé publique du serveur. Mais comment être certain que ce serveur est digne de confiance?

Lorsqu'une connexion TLS s'établit entre un client et un serveur, vient un moment où le serveur doit faire parvenir sa clé publique au client : cette clé est transmise en même temps qu'un ensemble d'informations qu'on appelle le certificat numérique. Ces informations sont par exemple le nom de l'entreprise, le nom de domaine associé, une adresse, un email, etc.

Pour la majorité des sites HTTPS, le certificat n'est pas émis par l'instance qui émet la clé publique, mais par une autorité de certification, qui est une entreprise qui atteste de l'authenticité de l'émetteur.

Les autorités de certification émettent elles-mêmes une paire de clés publique et privée, et distribuent leur clé publique dans un certificat. Les fabricants de logiciels incluent ces certificats "racine" dans leurs applications (par exemple Firefox, IE, etc.).

Une entreprise X désirant obtenir un certificat pour opérer un site HTTPS enverra ses coordonnées et sa clé publique à un autorité de certification, qui fera les vérifications nécessaires pour valider son identité. Si l'entreprise est digne de confiance, l'autorité émet un certificat qu'elle signe avec sa propre clé privée. Elle envoie ensuite ce certificat à l'entreprise.

Un client qui se connecte au site web de l'entreprise X reçoit donc ce certificat, qui comprend la clé publique de l'entreprise, des informations qui la concernent, et un message chiffré avec la clé privée de l'autorité de certification. Le client peut donc valider l'authenticité de la clé publique jointe au certificat s'il est capable de déchiffrer la signature avec la clé publique de l'autorité de certification.

Cette séquence d’évènements est représentée dans le schéma suivant :

![certificat](/420-313/images/certificat.png)

![Comprendre le chiffrement SSL / TLS avec des emojis (et le HTTPS)](https://youtu.be/7W7WPMX7arI?si=XLtBFf-m4X4ev2m8)

![Tutoriel Sécuriser son ordinateur : Les certificats de chiffrement](https://youtu.be/XXqkBPgz8hA?si=ECy15fSd79txa2fX)

# Exercice – Génération d’un certificat autosigné avec OpenSSL

En règle générale, lorsqu'on a besoin d'un certificat, on doit faire une demande auprès d'une autorité de certification ; mais dans certains cas exceptionnels (par exemple, un serveur intranet à accès privé, un serveur de courrier d'entreprise, etc.), on n'a pas besoin de faire attester l'authenticité de notre certificat. Dans ces cas, on signera numériquement nous-mêmes notre certificat. C’est ce qu’on appelle un certificat autosigné. OpenSSL fournit les outils nécessaires pour générer et signer nous-mêmes des certificats numériques.

## Générer la clé privée du serveur

La première étape consiste à générer la clé privée du serveur ; le certificat contiendra la clé publique correspondante. La commande est la suivante ; par défaut, la clé a une taille de 512 bits, mais ici, on la définit sur 1024 bits :

```shell
openssl genrsa -out certif.key 1024
```

## Générer la demande de certificat

Ensuite, nous générons un fichier contenant les informations nécessaires pour créer notre certificat :

```shell
openssl req -new -key certif.key -out certif.req
```

L'option `-new` spécifie que nous créons un nouveau certificat, ce qui entraînera l'affichage de questions pour l'utilisateur.

L'option `-key` indique la clé privée associée au certificat, car une clé publique est générée au moment de la création de la demande.

L'option `-out` détermine le nom du fichier de sortie.

Après avoir répondu à plusieurs questions pour identifier le serveur, vous pouvez constater que la demande est encodée en base-64. Pour observer le contenu de votre demande, vous pouvez exécuter la commande suivante :

```shell
openssl req -text -noout -verify -in server.req
```

Vous pouvez constater que la clé publique correspondant à votre clé privée est incluse dans la demande.

## Générer et signer le certificat

Enfin, la dernière étape consiste à générer le certificat de la manière suivante :

```shell
openssl x509 -req -days 365 -in certif.req -signkey certif.key -out certif.crt
```

- L'option `-x509` définit la norme de création de certificats utilisée.
- L'option `-req` signifie que nous traitons une demande de certificat existante.
- L'option `-days` définit la durée de validité de la signature.
- L'option `-signkey` spécifie la clé privée utilisée pour générer la signature.

Une fois cette commande exécutée, votre certificat sera généré. Vous pouvez afficher son contenu en utilisant la commande suivante :

```shell
openssl x509 -in certif.crt -text -noout
```
