+++
chapter = true
pre = "<b>8. </b>"
title = "XSS"
weight = 80
+++

### Module 8

# "Cross-Site Scripting" (XSS)

(source: https://portswigger.net/web-security/cross-site-scripting)

Le *cross-site scripting* (ou "XSS") est une vulnérabilité web qui permet à un attaquant de contrôler certaines parties du code javascript d'une application. Ceci donne la possibilité à l'attaquant de révéler des informations sur les gens qui utilisent un site (particulièrement celles qui servent à les identifier où les authentifier), et de se faire passer pour eux afin d'accéder à leurs données personnelles. Les attaquants peuvent voler des informations sensibles telles que des cookies de session, détourner des comptes utilisateur, afficher du contenu malveillant ou rediriger les utilisateurs vers des sites Web compromis.

## Fonctionnement

L'attaque XSS permet à un attaquant d'injecter du code malveillant (généralement du code javascript) dans les pages Web consultées par d'autres utilisateurs. L'injection de ce code malveillant peut se faire de différentes manières, mais elle vient souvent d'un mauvais filtrage des données entrées par l'utilisateur. Ce code malveillant s'exécute ensuite dans le navigateur de la victime.

Il y a 3 types de XSS.

#### XSS stockée

L'attaquant injecte du code malveillant qui est stocké sur le serveur et affiché ultérieurement aux utilisateurs qui accèdent à la page contaminée. Par exemple, imaginez un site qui conserve les commentaires des utilisateurs qui le visitent. Tous les futurs visiteurs du site verront les commentaires des utilisateurs passés. Si un de ces utilisateurs a laissé, en guise de commentaire, quelques instructions javascript entre des balises `<script>`, ces instructions seront exécutées chaque fois que quelqu'un visite la page des commentaires. 

#### XSS réfléchie

Le code malveillant est injecté dans une requête HTTP, puis renvoyé par le serveur dans la réponse, affectant les utilisateurs qui consultent cette réponse particulière. Par exemple, imaginez une page dont le contenu est déterminé par la valeur du paramètre `message` de l'url suivant:
```
https://insecure-website.com/status?message=Bonjour+le+monde
```
Le serveur reçoit cet url, prend la valeur du paramètre `message` et génère une page où cette valeur s'affiche:
```
<p>Le message est: Bonjour le monde</p>
```
Un attaquant pourrait facilement injecter du code javascript dans l'url afin que celui-ci s'exécute dans la page, par exemple si l'url est:
```
https://insecure-website.com/status?message=<script>alert("XSS réfléchie!")</script>
```
La page affichée contiendra le code javascript:
```
<p>Le message est: <script>alert("XSS réfléchie!")</script></p>
```
Il s'agit donc pour l'attaquant de construire une url qui exploite cette vulnérabilité puis d'inciter la victime à cliquer cette url.

#### XSS basée sur le DOM

Cette variante de XSS se produit lorsque le code JavaScript malveillant manipule le Document Object Model (DOM) d'une page Web du côté client. Cela se produit souvent lorsque le code JavaScript de la page interagit avec des données non sécurisées provenant de l'URL.

Dans l'exemple suivant, on utilise javascript pour lire le contenu d'un champ `<input>` dont le `id` est 'search' et on l'affiche entre deux balises HTML (par exemple des `<div>`) dont le `id` est 'results':

```js
var search = document.getElementById('search').value;
var results = document.getElementById('results');
results.innerHTML = 'Vous avez recherché: ' + search;
```
L'attaquant pourrait entrer, au lieu d'un terme de recherche normal, du code javascript malveillant qui serait ensuite adjoint au `<div id='results'>`:

