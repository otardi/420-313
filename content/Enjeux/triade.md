+++
chapter = true
pre = "<b>1.1 </b>"
title = "Ce qu'on protège"
date = 2023-06-21T20:24:05-04:00
draft = false
weight = 11
+++

La cybersécurité a pour but de protéger les _informations_, peu importe leur forme, autant lorsqu'elles sont stockées que lorsqu'elles circulent à travers les réseaux de communication.

Les informations dont disposent les systèmes informatiques des organisations (comme une entreprise, un hôpital, une association, etc.) sont essentielles à leur fonctionnement et doivent donc être protégées. On peut penser aux informations personnelles des personnes (employés, clients), aux données bancaires ou financières de ceux-ci, à tout ce qui touche la propriété intellectuelle (inventions, recherche et développement), en plus des données privées comme les courriels ou autres systèmes de messagerie. Dans certains cas même, la loi oblige les orgqnisations à protéger les données, comme les données médicales ou judiciaires. 

Cette protection touche à trois aspects des informations: 
+ la confidentialité
+ l'intégrité
+ la disponibilité

On nomme souvent ces trois axes la "triade de cybersécurité", regroupés sous l'acronyme _CIA_ (de l'anglais _Confidentiality, Integrity, Availability_). Dans cette section nous allons voir en quoi cela consiste, comment chacun peut être attaqué et comment on les protège.

-------------
## Confidentialité
> L’information doit être protégée afin d’être accessible uniquement à ceux qui y sont autorisés.  

Les informations n'ont pas toutes la même valeur: certaines peuvent être diffusées sans risque, mais d'autres doivent demeurer secrètes. Les informations contenues dans une page web qui décrit la recette d'un gâteau aux carottes ont moins besoin d'être protégées que celles qui se trouvent dans un échange de courriels entre un comptable et ses clients.

Les données utilisées par les entreprises peuvent être de nature publique ou privée. Parmi les données privées, celles qui requièrent d’être protégées par la confidentialité se divisent globalement en deux catégories : les données personnelles, qui regroupent toutes les informations permettant d’identifier de manière unique les individus, et les données sur l’entreprise qui révèlent ses opérations ou ses actifs. Quelques exemples : 

##### Données personnelles:
+ Numéro d’assurance sociale, d’employé, ou autre identifiant unique 
+ Données médicales 
+ Dossier judiciaire 
+ Données fiscales 
+ Numéros de carte de crédit 

##### Données d'entreprise
+ Projets de développement ou secrets industriels 
+ Données financières 
+ Informations sur les clients ou les fournisseurs 
+ Actifs matériels 

#### Niveaux de confidentialité
Ces données ont différents _niveaux de confidentialité_: certains sont accessibles par un grand nombre de personnes (par exemple, tous les employés de l'entreprise), d'autre par très peu d'individus. Pour s'assurer de respecter ces niveaux de confidentialité, des mesures de base doivent être appliquées. Ces mesures sont:

##### Authentification 
Chacun des utilisateurs d’un système informatique peut être identifié de manière unique et doit prouver son identité pour accéder au système. On utilise généralement _l'identifiant utilisateur_ à cette fin, et les _mots de passe_ servent à l'utilisateur pour prouver qu'il est bien celui qu'il prétend.

##### Autorisation 
Il faut déterminer quels utilisateurs ont accès à quelles informations, et la nature de ces permissions. Les systèmes de privilèges présents dans la plupart des systèmes informatiques servent à associer les utilisateurs aux informations qu'ils peuvent consulter. Par exemple, les permissions d'un système d'exploitation donnent à des utilisateurs et des groupes d'utilisateurs des droits de lecture, écriture ou exécution sur less fichiers du système.

##### Imputabilité (ou "traçabilité")
On doit pouvoir savoir à quelles données un utilisateur a accédé, pour combien de temps et quelles modifications il y a apportées. Les logiciels et systèmes informatiques qui contiennent des données sensibles disposent souvent de mécanismes de journalisation ("logs") qui permettent de retracer les accès à ces données.

#### Chiffrement 
Le *chiffrement* est une mesure de protection supplémentaire pour la confidentialité. Lorsqu'on chiffre des données, celles-ci deviennent impossibles à lire à moins de disposer de la *clé* qui permet de les déchiffrer. 

_BitLocker_ est un outil dans Windows qui permet de chiffrer les partitions d'un disque dur; _LUKS_ est un équivalent pour linux.

Des protocoles de communication comme TLS ou SSH utilisent le chiffement pour préserver la confidentialité des données qui circulent sur les réseaux.

#### Valeur des informations
En règle générale, la confidentialité d'une information va de pair avec sa valeur: par exemple, une entreprise qui développe un logiciel qui lui rapporte beaucoup d'argent essaiera d'en garder le code source secret. Un grand nombre d'attaques informatiques ont ainsi pour objectif de se procurer des données confidentielles car celles-ci peuvent rapporter beaucoup d'argent en étant utilisées ou vendues à des tiers.

Lorsque la confidentialité des données est brisée, il n’y a pas de retour en arrière possible : les informations ne sont plus secrètes, on peut uniquement se protéger contre les attaques futures. C’est pourquoi les fuites de données relatives aux clients de grandes entreprises, sans nécessairement affecter directement les entreprises elles-mêmes, peuvent porter un coup important sur la confiance envers celles-ci. 

#### Exemple d'une attaque sur la confidentialité
En cliquant sur le fichier _.docx_ attaché à un courriel, un employé a déclenché à son insu l’installation d’un « keylogger » sur son poste de travail qui enregistre tous les identifiants et mots de passe des sites qu’il visite. Cette liste d'identifiants de connexion est ensuite récupérée par l'attaquant qui peut accéder à tous les comptes en ligne de l'employé. Dans cet exemple, c'est la confidentialité des mots de passe qui est perdue.

-------------
## Intégrité
> L’information doit être authentique et fiable : on peut attester avec certitude de sa provenance, et elle n’a pas été modifiée de façon non-autorisée.

L'intégrité est reliée à la _qualité_ des données: elle est affectée par la dégradation ou la modification (volontaire ou non) des informations sur ses différents supports.

L'intégrité est souvent affectés par des défectuosités techniques: par exemple, des interférences dans le signal électrique ayant un effet sur la qualité de la communication dans un réseau, l'usure d'un disque qui rend illisibles les informations das un fichier, etc.

#### Exemples de bris d'intégrité
+ Le logiciel du senseur de pression artérielle utilisé par le département de cardiologie d’un hôpital contient un bogue qui _affiche des informations erronées à l'écran_ lors de son utilisation. 
+ Une erreur dans les permissions d’un fichier a pour effet qu’un utilisateur non autorisé _modifie la page d’accueil_ du site d’une entreprise. 
+ Le disque dur d'un portable a des défectuosités et certains fichiers qu'il contient ne sont _plus récupérables_.

#### Contrôle des accès
Comme pour la confidentialité, le contrôle des accès est une manière de protéger l'intégrité des informations. En effet, un accès non autorisé à un système ou à un fichier peut être un problème d’_intégrité_ autant que de _confidentialité_: si un utilisateur non autorisé lit des données auxquelles il n’a pas d’accès, on a un bris de confidentialité; s’il modifie ces données, on a un bris d’intégrité.

#### Fonctions de hachage
On utilise ces fonctions pour vérifier l'intégrité d'un fichier ou d'une communication. Une fonction de hachage est le résultat d'un calcul mathématique sur les données contenues dans un fichier. 

Par exemple, pour un fichier texte qui contient la phrase "Un paquet d'instructrices cambriolent un ouragan.", le résultat d'un _hachage MD5_ est `7a8a851bf4c99e729e56ed6c2eff48da`. La particularité de ce calcul est que son résultat est unique pour chaque fichier. Autrement dit, si le fichier change, le résultat changer lui aussi. Si en effet on change une seule lettre dans la phrase précédente, le résultat sera complètement différent: "Un paquet d'instructrices cambriolent un ouragax." donne `df95caff6efa566355915993f3c9bf79`.

Une fonction de hachage est donc l'équivalent d'une empreinte digitale pour un fichier: il permet de l'identifier et si son résultat change, cela signifie que le contenu du fichier a changé. Un peut donc vérifier l'intégrité d'un fichier en comparant leur valeur de hachage prise à des moments différents.

Les fonctions de hachage sont utilisées dans les signatures digitales et les certificats numériques, d'autre systèmes plus complexes pour valider l'intégrité (et aussi l'authenticité) des informations.

-------------
## Disponibilité
> L’information doit être accessible aux utilisateurs autorisés au moment où ils en ont besoin.

La disponibilité peut être compromise par une attaque directe mais aussi par des pannes ou par une mauvaise gestion des ressources.

Les attaques qui touchent la disponibilité des systèmes visent à rendre ceux-ci inaccessibles à leurs utilisateurs, ce qui peut paralyser les organisations ou encore leur faire perdre des revenus. L’objectif peut être politique ou social, mais il est souvent financier. Les attaques les plus courantes sur la disponibilité sont les _DDoS_ et les rançongiciels. 

Des problèmes de disponibilité peuvent également survenir lors de défauts matériels ou logiciels ou encore d’erreurs humaines, lorsque ceux-ci causent des pannes par exemples. 

Il existe un lien entre _intégrité_ et _disponibilité_. En effet, si par exemple une défectuosité technique modifie quelques octets des fichiers contenus dans une clé USB, on a un bris d'intégrité de ces fichiers. Et si ces fichiers sont trop fortement modifiés, il pourrait être impossible de les ouvrir, ce qui serait un impact sur la disponibilité.

#### Redondance
La méthode la plus répandue pour protéger la disponibilité des informations est d'éliminer ce qu'on appelle les **points de faille uniques**.

Dans un système informatique, un point de faille unique est une composante ou un logiciel qui, s'il cesse de fonctionner, empêche aussi l'ensemble du système de fonctionner. Par exemple, si tous les documents importants d'une entreprise sont stockés sur le disque dur d'un seul serveur de fichiers, une panne de ce serveur entraîne une perte de disponiblilité de ces fichiers.

Idéalement, lorsqu'on a des données importantes à gérer (comme par exemple dans une salle de serveurs) il est souhaitable d'avoir de la redondance à tous les niveaux: alimentation électrique, composantes réseau, serveur, supports de stockage...).

#### Exemples de bris de disponibilité
+ Une attaque par déni de service empêche les utilisateurs de Netflix du nord-est des USA d’utiliser la plateforme. 
+ Les opérateurs d’un marché illégal sur le dark web sont victimes d’une attaque par déni de service et doivent payer une somme au responsable de cette attaque pour qu’il cesse ses activités. 
+ Lors de la mise à niveau d’un serveur de courriel, on change les disques durs de ce dernier et le système reste indisponible durant 2 heures le temps de copier les données des anciens disques sur les nouveaux. 
+ Une personne inconnue remplace la page d’accueil de Omnivox du collège par une photo de chat. 

{{% notice warning "Remarque" %}}
Confidentialité, intégrité et disponibilité ne sont pas nécessairement isolées les unes des autres. Des problèmes matériels ou logiciels peuvent avoir des impacts sur plus d’un de ces trois aspects; et les attaques informatiques peuvent comprendre plusieurs phases où chacune affecte un aspect différent.

Par exemple:
+ Un acteur malveillant accède au compte _root_ d’un serveur de courrier, télécharge la base de données (confidentialité) des messages et efface le disque dur du serveur (disponibilité).   
+ Une attaque de _ransomware_ encrypte les données de votre disque dur et vous demande $1000 pour le déchiffrer. Cette attaque touche l’intégrité car le contenu de votre disque a changé, et aussi la disponibilité car vous ne pouvez plus accéder à vos informations.

{{% /notice %}}

---------------------
## À consulter
+ [What is the CIA Triad? (IBM Technology)](https://youtu.be/kPPFNrlN3zo)
+ [What is the CIA Triad? (Coursera)](https://www.coursera.org/articles/cia-triad)