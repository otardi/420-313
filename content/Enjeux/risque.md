+++
chapter = true
pre = "<b>1.2 </b>"
title = "De quoi on se protège"
date = 2023-06-27T14:16:47-04:00
draft = false
weight = 15
+++

Dans la section précédente nous avons vu les notions de confidentialité, d'intégrité et de disponibilité, qui sont les trois aspects de l'information que la cybersécurité doit protéger. 

Les problèmes qui affectent ces trois aspects peuvent avoir des origines diverses. Dans cette section nous verrons dans quelles catégories on les regroupe afin de mieux les comprendre et les analyser. Pour ce faire nous allons aborder trois autres notions fondamentales:
+ Les vulnérabilités
+ Les menaces
+ Le risque

------------
## Vulnérabilités
> Tout état d’un système qui peut être **exploité** dans le but de compromettre la confidentialité, l’intégrité ou la disponibilité des informations qu’il contient. 

Une vulnérabilité désigne généralement une faiblesse, une faille dans un système informatique. Elles peuvent être de trois types:
+ **Logicielles**: par exemple, un bogue dans une application qui peut être exploité pour divulguer des informations sensibles. Ce sont des vulnérabilités dans le code source ou la configuration des programmes utilisés.
+ **Matérielles**: par exemple, un mécanisme de protection comme une serrure ou une barrière qui est en panne, défectueux ou carrément absent. Elles touchent à l'état physique des composantes matérielles utilisées dans une infrastructure.
+ **D'origine humaine**: par exemple, des utilisateurs mal informés qui ouvrent tous les fichiers en annexe à leurs courriels. Elles découlent des actions des utilisateurs ou des administrateurs du système informatique.

En cybersécurité on se concentre généralement sur les vulnérabilités logicielles car elles sont souvent plus difficiles à détecter et peuvent avoir des impacts énormes. 

#### "Exploits"
Les chercheurs en cybersécurité (souvent employés par les grandes entreprises comme Microsoft, Google etc.) sont toujours à l'affût de failles et de bogues qui peuvent être exploités dans une foule de logiciels.

Lorsqu'une vulnérabilité logicielle est découverte, elle est toujours associée à un ***exploit*** (« exploitation »), qui est un programme permettant de démontrer cette vulnérabilité. Évidemment, cet *exploit* peut être utilisé par des personnes mal intentionnées; c'est pourquoi en général les chercheurs en cybersécurité contactent les fabricants du logiciel vulnérable _avant_ d'annoncer publiquement la vulnérabilité. Ceci donne le temps au fabricant de développer des correctifs et d'envoyer une mise-à-jour aux utilisateurs.

Le site [Exploit Database](https://www.exploit-db.com/) répertorie les différents _exploits_ pour un grand nombre de vulnérabilités logicielles connues.

#### "Zero-day"
Une vulnérabilité logicielle peut exister longtemps avant d’être repérée et corrigée par le fabricant. Durant cette période, elle peut être exploitée par des attaquants qui l'auraient détectée plus tôt. On nomme **exploitation du jour zéro** (ou _Zero-Day exploit_) lorsqu’on découvre l'existence d'un programme malicieux visant à exploiter une vulnérabilité avant que la vulnérabilité elle-même ait été officiellement découverte. Ceci signifie que le fabricant doit développer en urgence un correctif en sachant que la vulnérabilité a déjà été exploitée.  

#### CVE
L'organisme [NIST](https://www.nist.gov/) tient à jour une liste des vulnérabilités logicielles connues, la [National Vulnerability Database](https://nvd.nist.gov/). Les vulnérabilités sont identifiées par un code "CVE" (qui signifie _common vulnerabilities and exposure_) suivi de l'année de découverte et d'un nombre, par exemple `CVE-2022-3422`.

Deux autres sites permettent de faire des recherches sur les CVE:
+ [CVE (Mitre Corporation)](https://www.cve.org/)
+ [CVE Details](https://www.cvedetails.com/)

--------------
## Menaces
> Activité, évènement ou logiciel ayant le potentiel de compromettre la sécurité d’un système informatique en affectant la confidentialité, l’intégrité ou la disponibilité des informations qu’il contient.  

La notion de menace est très générale: elle inclut n'importe quel évènement qui peut avoir un impact sur la cybersécurité. Ainsi un tremblement de terre ou un orage peut être perçu comme une menace. 

La plupart du temps cependant lorsqu'on parle de menace on désigne des personnes ou des groupes qui sont à la recherche de vulnérabilités dans le but de les exploiter à des fins malveillantes. On nomme **acteurs** les individus ou groupes d’individus à l’origine des menaces. On peut les classer en 5 groupes, chacun ayant des motivations qui leur sont propres : 

| Entité | Motivation | Type d'attaque |
| -- | -- | -- |
| États | guerre, espionnage |Attaques sur les infrastructures, vol de données |
| Cybercriminels | argent | Minage de cryptomonnaie, vol d’identité, botnets, ransomware  |
| Hacktivistes | causes idéologiques | Déni de service, vol d’identité, divulgation d’information  |
| Groupes terroristes | causes idéologiques | Déni de service, vol d’identité, divulgation d’information  |
| Individus | réputation, amusement, argent, vengeance | Minage de cryptomonnaie, vol d’identité, destruction de données, divulgation d’information  |


## Risque
> Mesure permettant de quantifier l’impact d’un évènement donné sur les opérations et les actifs d’une organisation.

Calculer le rsique d'un évènement permet d'en estimer la gravité, et ainsi de prioriser les mesures de sécurité à prendre.

Deux éléments sont centraux à la notion de risque: la **probabilité** et l’**impact**. On représente souvent le calcul du risque avec la formule suivante: 


###### **Risque** = Probabilité d’un évènement **x** gravité des conséquences

On utilise cette formule pour quantifier le risque de différentes situations:

+ *Un évènement assez probable et dont l'impact est important aura un haut niveau de risque.* Par exemple, la perte ou le vol d'un disque dur amovible contenant des secrets militaires.
+ *Un évènement peu probable et dont l'impact est négligeable aura un faible niveau de risque.* Par exemple, le fait de deviner le mot de passe de 24 caractères du compte _root_ d'un serveur qui ne contient aucune donnée importante.
+ *Un évènement rare mais très grave aura environ le même niveau de risque qu'un évènement très probable mais anodin.* Par exemple, un incendie dans la salle des serveurs a le même niveau de risque qu'un utilisateur qui oublie son mot de passe.

On peut utiliser une matrice comme la suivante pour évaluer le risque:

![matrisque](/420-313/images/matrisque2.png)

#### Facteurs de risque
Tout ce qui peut avoir un effet sur la probabilité ou l'impact d'un évènement est un _facteur de risque_. 

Imaginons par exemple un risque d'inondation dans une salle de serveurs. La *probabilité* (et donc le niveau de risque) de cet évènement augmente si des tuyaux de plomberie passent dans le plafond. La présence de canalisations dans le plafond est donc un facteur de risque. 

Pour un autre exemple, imaginons une entreprise qui contient des informations confidentielles sur ses clients (numéros de cartes de crédit, adresses, courriels, etc.). La probabilité que ces données soient volées lors d'une cyberattaque n'est pas nulle. L'*impact* de cette cyberattaque serait toutefois moins grand si les données étaient chiffrées - les voleurs, même en possession des données, ne pourraient alors pas les déchiffrer. Ainsi, le fait que les données ne soient pas chiffrées sur les serveurs de l'entreprise est un facteur de risque.

#### Gestion du risque
On dit souvent que « le risque zéro n'existe pas » : il est en effet impossible d’éliminer tous les risques qui existent. L’objectif, lorsqu’on identifie un risque, consiste donc à savoir comment on veut le traite, et quelles mesures doivent être prises à cette fin. Il y a 5 manière de gérer le rsique: 

1. **Éviter le risque**: Cesser (ou renoncer à) l’activité qui comporte un risque. 
2. **Accepter le risque**: Poursuivre l’activité sans entreprendre de mesures particulières. 
3. **Mitiger le risque**: Implanter des mesures qui auront pour effet de diminuer la probabilité et/ou l’impact associés au risque. On tente ici de diminuer un des _facteurs de risque_.
4. **Transférer le risque**: Donner la responsabilité de gérer le risque à un tiers parti. Souscrire à une assurance est un exemple de transfert de risque (on paie une entreprise pour obtenir un montant qui diminue l'impact d'un évènement).
5. **Partager le risque**: S’associer à d’autres entités afin de diminuer les coûts associés à la gestion du risque. 

--------------------
## À consulter
+ [Introduction à l'environnement de cybermenaces (Gouv. Canada)](https://www.cyber.gc.ca/fr/orientation/introduction-lenvironnement-de-cybermenaces)
+ [Qu'est-ce qu'un "Zero-Day" (ESET)](https://www.eset.com/fr/cybermenaces/faille-zero-day/)
+ [Gestion du risque (Red Hat)](https://www.redhat.com/fr/topics/management/what-is-risk-management)
