+++
chapter = true
pre = "<b>6. </b>"
title = "SSH et fail2ban"
weight = 60
+++

### Module 6

# SSH et fail2ban

## Les attaques par “force brutale”

Une attaque par force brutale consiste à énumérer de manière systématique tous les mots de passe possibles jusqu'à trouver le bon. C'est une méthode qui ne nécessite aucune connaissance préalable sur le mot de passe visé.

On peut utiliser ce type d’attaquepour deviner les mots de passe des utilisateurs d’un système (linux, Windows ou autre). Dans ca cas, l'attaquant procède de la façon suivante :

+ Il récupère une copie locale du fichier système contenant les mots de passe 
+ Il écrit un script ou utilise un outil qui va générer toutes les combinaisons possibles de caractères pour former des mots de passe 
+ Il hache chaque mot de passe généré et compare le résultat avec les hachages contenus dans le fichier 
+ Dès qu'un hachage correspond, il a trouvé le mot de passe en clair 
  
C'est une attaque rapide car elle ne nécessite pas d'interaction avec la cible.

Dans les cas où on cherche le mot de passe d’un système distant, 'lattaque est plus "lente" car à chaque tentative de mot de passe généré, on doit se connecter au service ciblé (ssh, ftp, etc.) et analyser la réponse pour savoir si la tentative a réussi ou échoué.

L'avantage est que l'attaquant n'a pas besoin d'accéder au fichier de hachages, mais l'inconvénient est que l'attaque est plus longue car elle dépend de la réactivité du service cible pour obtenir une rétroaction après chaque tentative. Dans les deux cas, le principe reste le même : tester systématiquement toutes les combinaisons possibles jusqu'à trouver le mot de passe, sans connaissance préalable sur ce dernier.

## Hydra

Puisque ce dernier genre d’attaque demande beaucoup de temps, il existe des outils permettant aux attaquants d’automatiser des tentatives de connexion. *Hydra* est un de ces outils: il est compatible avec plusieurs services (mysql, mssql, postgresql, svn, smb, snmp, cisco, ftp, forms https…), dont SSH.

Le principe de base est simple: il s'agit de créer un fichier texte qui contient un mot de passe par ligne, puis on passe à la commande les options suivantes:

+ Le fichier des mots de passe
+ L'identifiant de connexion ("login") dont on veut devner le mot de passe
+ L'adresse IP de la cible
+ Le service ou protocole de communication
  
Par exemple, pour essayer tous les mots de passe dans le fichier `mdp.txt` pour l'utilisateur *sami* dans une connexion SSH vers l'hôte `192.168.10.10`, la commande sera:
```
hydra -l sami -P mdp.txt 192.168.10.10 ssh
```

Il y a plusieurs options à la commande *hydra*; notamment:
+ **-L**: Si on veut tester plus d'un utilisateur, on met leurs noms dans un fichier texte et on nomme ce fichier texte après cette option.
+ **-c**: Permet de spécifier un nombre de secondes après chaque essai. L'attaque est ainsi plus discrète. 

#### Démonstration

Créez deux VM Debian. Une sera l'atttaquant, l'autre sera la cible.

Sur la VM qui attaque, installez *hydra* avec la commande suivante:
```
sudo apt update
sudo apt install hydra
```

Créez ensuite un fichier qui contient plusieurs mots (1 par ligne), dont un est le mot de passe d'un utilisateur sur le VM cible. Lancez ensuite la commande hydra comme dans l'exemple plus haut, en remplaçant l'adresse IP par celle de la victome est *sami* par le nom d'un utilisateur sur la cible.

#### Défenses

Les moyens pour diminuer les risques associés aux attaques par force brutale du service SSH sont les suivants:
+ Empêcher les connexions de l'utilisateur ***root*** à distance
+ Autoriser uniquement les connexions par clé publique (désactiver l'authentification par mot de passe)
+ Installer *fail2ban*

## *fail2ban*

Le programme *fail2ban* est un service qui observe les tentatives de connexions et bloque les adresses IP temporairement après un certain nombre de tentatives ratées. Il n'élimine pas entièrement le risque d'une attaque, mais il ralentit celles-ci très sérieusement.

À la base, il bloque les adresses IP pendant 10 minutes après 5 tentatives de connexion dans une période de 10 minutes. Mais ces valeurs peuvent être changées et il contient plusieurs paramètres de configuration permettant d'adapter son comportement à de nombreux scénarios.

Les adresses IP bloquées sont conservées dans des *prisons* ("jails") qui portent le nom du service pour lequel des tentatives de connexions ont été détectées. Par exemple, les connexions SSH bloquées seront dans une prison nommée "ssh".

#### Installation

Sur la VM cible, installez *fail2ban* avec la commande suivante:
```
sudo apt update
sudo apt install fail2ban
```

#### Configuration

Le fichier pour configurer fail2ban est `/etc/fail2ban/jail.conf`.

Les directives principales de ce fichier sont:
| Paramètre | Utilité | Valeur par défaut |
|---|---|---|
| `maxretry` | Le nombre de tentatives après lesquelles un hôte sera bloqué. | 5 |
| `bantime` | La durée de la période (en secondes, ou en minutes avec **m**) où une adresse sera bloquée. La valeur **-1** correspond à un blocage permanent. | 10m |
| `findtime` | La durée de la période (en secondes, ou en minutes avec **m**) pendant laquelle on compte les tentatives de connexions d’un hôte. | 10m |
| `ignoreip` | Une liste d’adresses d’hôtes ou de réseaux, séparées par des virgules, qui ne seront jamais bloqués. | 127.0.0.1/8 |

Si on souhaite changer ces paramètres il est recommandé de le faire dans un fichier à part nommé `/etc/fail2ban/jail.d/defaults-debian.local` dans lequel on définit les nouvelles valeurs et on active le service. Par exemple, pour bloquer les adresses qui essaient de se connecter 3 fois dans une période de 15 minutes (et conserver la période de 10 minutes de blocage), on doit mettre les informations suivantes dans le fichier: 
```
[DEFAULT] 
findtime = 15m 
maxretry = 3 
 
[sshd] 
enabled = true  
```

Après avoir modifié le fichier de configuration, le service devra être redémarré avec la commande suivante:
``` 
systemctl fail2ban restart 
```

#### Commandes utiles

Pour affiche la liste des "prisons":
```
fail2ban-client status
```

Pour afficher les adresses bloquées pour une prison donnée (*ssh* dans cet exemple):
```
fail2ban-client status ssh
```

Pour supprimer une adresse IP d'une prison (c'est-à-dire la débloquer):
``````
fail2ban-client set ssh unbanip 192.168.10.10
```

